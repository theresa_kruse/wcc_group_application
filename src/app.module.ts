import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TripsController } from './trips/trips.controller';
import { DriversController } from './drivers/drivers.controller';

@Module({
  imports: [],
  controllers: [AppController, TripsController, DriversController],
  providers: [AppService],
})
export class AppModule {}
