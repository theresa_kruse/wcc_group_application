import { Body, Controller, Get, Post, Put } from '@nestjs/common';

interface Car { car_id: string; driver_name: string;}

let initialDrivers = [{car_id: 'XX-200-R', driver_name: 'Ernesto'}]

@Controller('drivers')
export class DriversController {
    
    private readonly drivers: Car[] = initialDrivers;

    @Get()
    findAll(): Car[] {
        return this.drivers;
    }

    @Post()
    create(@Body() driver: Car) {
        var included = 0
        for (let i = 0; i < this.drivers.length; i++) {
            if (this.drivers[i].car_id === driver.car_id) {
                this.drivers[i].driver_name = driver.driver_name
                included = 1
            } 
        }
        if (included === 0) {
            this.drivers.push(driver)
        }
    }

    @Put()
    update(@Body() driver: Car) {
        for (let i = 0; i < this.drivers.length; i++) {
            if (this.drivers[i].car_id === driver.car_id) {
                this.drivers[i].driver_name = driver.driver_name
            }
        }
    }
}