import { Injectable } from "@nestjs/common";

interface Car { car_id: string; driver_name: string;}

let initialDrivers = [{car_id: 'XX-200-R', driver_name: 'Ernesto'}]

@Injectable()
export class DriversService {
    private readonly drivers: Car[] = initialDrivers;

    findAll(): Car[] {
        console.log(this.drivers)
        return this.drivers;
    }

    create(driver:Car) {
        this.drivers.push(driver)
    }

    update(driver:Car) {
        var included = 0
        for (let i = 0; i < this.drivers.length; i++) {
            if (this.drivers[i].car_id == driver.car_id) {
                this.drivers[i].driver_name = driver.driver_name
                included = 1
            } 
        }
    }
}