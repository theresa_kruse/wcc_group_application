import { Body, Controller, Get, Post, Put } from '@nestjs/common';

interface Trip { destination: string; license: string; duration: number;}

let initialTrips = [{destination: 'Lisbon', license: 'XX-200-R', duration: 30}]

@Controller('trips')
export class TripsController {
    
    private readonly trips: Trip[] = initialTrips;

    @Get()
    findAll(): Trip[] {
        return this.trips;
    }

    @Post()
    create(@Body() trip: Trip) {
        this.trips.push(trip)
    }
}

