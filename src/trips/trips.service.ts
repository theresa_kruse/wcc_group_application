import { Injectable } from "@nestjs/common";

export interface Trip { destination: string, license: string, duration: number }

let initialTrips = [{destination: 'Lisbon', license: 'XX-200-R', duration: 30}]

@Injectable()
export class TripsService {
    private readonly trips: Trip[] = initialTrips;

    findAll(): Trip[] {
        console.log(this.trips)
        return this.trips;
    }

    create(trip:Trip) {
        this.trips.push(trip)
    }
}