# Project members
Jacob Lind - 32614
Theresa Kruse - 43891
Arne Wawers - 44694

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

We are using a REST Api. 

GET - fetch the data from the JSON
POST - if the driver already exists, it will be updated, otherwise a new one is created
PUT - only updates the driver if it already exists

# Comments on the development

We created the project using Sourcetree and Gitlab. This way we could asure that we use different branches and could work on our code in parallel.
We modified the code from the lecture and the initial quizzes and updated the code accordingly. 
Our main communication channel was Teams.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
